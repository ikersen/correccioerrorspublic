package input01;

import javax.swing.JOptionPane;

public class Input01 {
    public static void main(String[] args) {
        //Create a JOptionPane.
    	//Store the input as a String and print it.
        String textInput = JOptionPane.showInputDialog("Escribe algo: ");
        System.out.println(textInput);
      
        //Parse the input as an int.
        //Print its value +1
        int numInput = Integer.parseInt(textInput);
        System.out.println(++numInput);
        
        
        //Try creating a dialog, parsing it, and initializing an int in a single line.
        //You should have only one semicolon (;) in this line.
        int numInput2 = Integer.parseInt(JOptionPane.showInputDialog("Escribe algo: "));
        System.out.println(numInput2);
    }
}
